package com.uni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class HSP1_9 {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HSP1_9_Driver(), args);
        System.exit(res);
    }

    public static class HSP1_9_Driver extends Configured implements Tool {

        @Override
        public int run(String[] args) throws Exception {
            JobConf conf = new JobConf(getConf(), HSP1_9_Driver.class);
            conf.setJobName("hsp1_4_1");

            Path outputPath = new Path(args[1]);
            FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
            fs.delete(outputPath, true);

            FileInputFormat.setInputPaths(conf, new Path(args[0]));
            FileOutputFormat.setOutputPath(conf, new Path(args[1]));

            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(Text.class);
            conf.setMapperClass(CustomMapper.class);
            conf.setReducerClass(CustomReducer.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf);

            return 0;
        }
    }

    public static class CustomMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, Text> outputCollector, Reporter reporter) throws IOException {
            if (key.get() == 0 || value.toString().contains("Transaction_date")) return;

            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");

            String country = singleRowData[7].trim();
            String name = singleRowData[4].trim();
            String loginDate = singleRowData[9].trim();

            if (country.isEmpty() || name.isEmpty() || loginDate.isEmpty()) return;

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy HH:mm");

            try {
                dateFormat.parse(loginDate);
            } catch (ParseException e) {
                return;
            }

            outputCollector.collect(new Text(country + ","), new Text(name + "," + loginDate));
        }
    }

    public static class CustomReducer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
        @Override
        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            String recently = "";
            Date recentlyDate = new Date(Long.MIN_VALUE);

            while (values.hasNext()) {
                String[] singleRowData = values.next().toString().split(",");

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy HH:mm");

                Date date;

                try {
                    date = dateFormat.parse(singleRowData[1]);
                } catch (ParseException e) {
                    return;
                }

                if (date.compareTo(recentlyDate) > 0) {
                    recently = singleRowData[0];
                    recentlyDate = date;
                }
            }

            output.collect(key, new Text(recently));
        }
    }
}