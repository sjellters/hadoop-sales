package com.uni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class HSP1_3 {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HSP1_3_Driver(), args);
        System.exit(res);
    }

    public static class HSP1_3_Driver extends Configured implements Tool {

        @Override
        public int run(String[] args) throws Exception {
            JobConf conf = new JobConf(getConf(), HSP1_3_Driver.class);
            conf.setJobName("hsp1_3_1");

            Path outputPath = new Path(args[2]);
            FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
            fs.delete(outputPath, true);

            FileInputFormat.setInputPaths(conf, new Path(args[0]));
            FileOutputFormat.setOutputPath(conf, new Path(args[1]));

            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(IntWritable.class);
            conf.setMapperClass(CustomMapper.class);
            conf.setReducerClass(CustomReducer.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf);

            JobConf conf2 = new JobConf(getConf(), HSP1_3_Driver.class);
            conf.setJobName("hsp1_3_2");

            FileInputFormat.setInputPaths(conf2, new Path(args[1]));
            FileOutputFormat.setOutputPath(conf2, new Path(args[2]));

            conf2.setOutputKeyClass(Text.class);
            conf2.setOutputValueClass(Text.class);
            conf2.setMapperClass(CustomMapper2.class);
            conf2.setReducerClass(CustomReducer2.class);
            conf2.setInputFormat(TextInputFormat.class);
            conf2.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf2);

            Path intermediateOutputPath = new Path(args[1]);
            FileSystem fs2 = FileSystem.get(new URI(intermediateOutputPath.toString()), conf);
            fs2.delete(intermediateOutputPath, true);

            return 0;
        }
    }

    public static class CustomMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

        private final static IntWritable ONE = new IntWritable(1);

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> outputCollector,
                        Reporter reporter) throws IOException {
            if (key.get() == 0 || value.toString().contains("Transaction_date")) return;

            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");

            String country = singleRowData[7].trim();
            String paymentType = singleRowData[3].trim();

            if (country.isEmpty() || paymentType.isEmpty()) return;

            outputCollector.collect(new Text(country + "," + paymentType + ","), ONE);
        }
    }

    public static class CustomMapper2 extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, Text> outputCollector, Reporter reporter) throws IOException {
            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");

            outputCollector.collect(new Text(singleRowData[0] + ","),
                    new Text(singleRowData[1] + "," + singleRowData[2].trim()));
        }
    }

    public static class CustomReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        public void reduce(Text key, Iterator<IntWritable> iterator,
                           OutputCollector<Text, IntWritable> outputCollector, Reporter reporter) throws IOException {
            int total = 0;
            while (iterator.hasNext()) {
                IntWritable value = iterator.next();
                total += value.get();
            }

            outputCollector.collect(key, new IntWritable(total));
        }
    }

    public static class CustomReducer2 extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            List<ReducerDO> valuesList = new ArrayList<>();

            while (values.hasNext()) {
                String[] singleRowData = values.next().toString().split(",");

                String currentPaymentType = singleRowData[0].trim();
                String currentTotal = singleRowData[1].trim();

                int priceInt;

                try {
                    priceInt = Integer.parseInt(currentTotal);
                } catch (NumberFormatException ex) {
                    priceInt = 0;
                }

                valuesList.add(new ReducerDO(currentPaymentType, priceInt));
            }

            valuesList.sort(Comparator.comparingInt(ReducerDO::getValue));

            try {
                valuesList.remove(0);
                valuesList.remove(valuesList.size() - 1);
            } catch (IndexOutOfBoundsException ex) {
                System.out.println("List is probably empty!");
            }

            for (ReducerDO reducerDO : valuesList) {
                output.collect(key, new Text(reducerDO.getKey()));
            }
        }
    }

    private static class ReducerDO {
        private final String key;
        private final int value;

        public ReducerDO(String key, int value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public int getValue() {
            return value;
        }

    }
}