package com.uni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

public class HSP2_5 {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HSP1_1_Driver(), args);
        System.exit(res);
    }

    public static class HSP1_1_Driver extends Configured implements Tool {

        @Override
        public int run(String[] args) throws Exception {
            JobConf conf = new JobConf(getConf(), HSP1_1_Driver.class);

            conf.setJobName("hsp1_1");

            Path outputPath = new Path(args[1]);
            FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
            fs.delete(outputPath, true);

            FileInputFormat.setInputPaths(conf, new Path(args[0]));
            FileOutputFormat.setOutputPath(conf, new Path(args[1]));

            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(Text.class);
            conf.setMapperClass(CustomMapper.class);
            conf.setReducerClass(CustomReducer.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf);

            return 0;
        }
    }

    public static class CustomMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

        public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            if (key.get() == 0) {
                return;
            }
            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");
            output.collect(new Text(singleRowData[7].trim() + ',' + singleRowData[5].trim()),
                    new Text(singleRowData[10] + ',' + singleRowData[11]));
        }
    }

    public static class CustomReducer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        public void reduce(Text t_key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            double averageLat = 0.0;
            double averageLng = 0.0;
            int i = 0;
            while (values.hasNext()) {
                // replace type of value with the actual type of our value
                String[] vals = values.next().toString().split(",");

                double lat = Double.parseDouble(vals[0]);
                double lng = Double.parseDouble(vals[1]);

                averageLat += lat;
                averageLng += lng;
                i++;
            }
            averageLat = averageLat / i;
            averageLng = averageLng / i;

            output.collect(t_key,
                    new Text("avgLat: " + averageLat + ", avgLng: " + averageLng));
        }
    }
}