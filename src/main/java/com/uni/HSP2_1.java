package com.uni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class HSP2_1 {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HSP2_2_Driver(), args);
        System.exit(res);
    }

    public static class HSP2_2_Driver extends Configured implements Tool {

        @Override
        public int run(String[] args) throws Exception {
            JobConf conf = new JobConf(getConf(), HSP2_2_Driver.class);
            conf.setJobName("hsp2_1_1");

            Path outputPath = new Path(args[1]);
            FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
            fs.delete(outputPath, true);

            FileInputFormat.setInputPaths(conf, new Path(args[0]));
            FileOutputFormat.setOutputPath(conf, new Path(args[1]));

            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(IntWritable.class);
            conf.setMapperClass(CustomMapper.class);
            conf.setReducerClass(CustomReducer.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf);

            return 0;
        }
    }

    public static class CustomMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> outputCollector,
                        Reporter reporter) throws IOException {
            if (key.get() == 0 || value.toString().contains("Transaction_date")) return;

            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");

            String city = singleRowData[5].trim();
            String product = singleRowData[1].trim();
            String price = singleRowData[2].trim();

            if (city.isEmpty() || product.isEmpty() || price.isEmpty()) return;

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy HH:mm");

            Date currentDate;

            try {
                currentDate = dateFormat.parse(singleRowData[0]);
            } catch (ParseException e) {
                return;
            }

            if (currentDate.getMonth() != Calendar.JANUARY) return;

            int priceInt;

            try {
                priceInt = Integer.parseInt(price);
            } catch (NumberFormatException e) {
                return;
            }

            System.out.println(city + product + priceInt);

            outputCollector.collect(new Text(city + "," + product + ","), new IntWritable(priceInt));
        }
    }

    public static class CustomReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        public void reduce(Text key, Iterator<IntWritable> iterator,
                           OutputCollector<Text, IntWritable> outputCollector, Reporter reporter) throws IOException {
            int total = 0;
            while (iterator.hasNext()) {
                IntWritable value = iterator.next();
                total += value.get();
            }

            outputCollector.collect(key, new IntWritable(total));
        }
    }
}