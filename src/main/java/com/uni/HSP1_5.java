package com.uni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

public class HSP1_5 {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HSP1_5_Driver(), args);
        System.exit(res);
    }

    public static class HSP1_5_Driver extends Configured implements Tool {

        @Override
        public int run(String[] args) throws Exception {
            JobConf conf = new JobConf(getConf(), HSP1_5_Driver.class);
            conf.setJobName("hsp1_2_1");

            Path outputPath = new Path(args[2]);
            FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
            fs.delete(outputPath, true);

            FileInputFormat.setInputPaths(conf, new Path(args[0]));
            FileOutputFormat.setOutputPath(conf, new Path(args[1]));

            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(IntWritable.class);
            conf.setMapperClass(CustomMapper.class);
            conf.setReducerClass(CustomReducer.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf);

            JobConf conf2 = new JobConf(getConf(), HSP1_5_Driver.class);
            conf.setJobName("hsp1_2_2");

            FileInputFormat.setInputPaths(conf2, new Path(args[1]));
            FileOutputFormat.setOutputPath(conf2, new Path(args[2]));

            conf2.setOutputKeyClass(Text.class);
            conf2.setOutputValueClass(Text.class);
            conf2.setMapperClass(CustomMapper2.class);
            conf2.setReducerClass(CustomReducer2.class);
            conf2.setInputFormat(TextInputFormat.class);
            conf2.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf2);

            Path intermediateOutputPath = new Path(args[1]);
            FileSystem fs2 = FileSystem.get(new URI(intermediateOutputPath.toString()), conf);
            fs2.delete(intermediateOutputPath, true);

            return 0;
        }
    }

    public static class CustomMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> outputCollector,
                        Reporter reporter) throws IOException {
            if (key.get() == 0 || value.toString().contains("Transaction_date")) return;

            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");

            outputCollector.collect(new Text(singleRowData[7] + "," + singleRowData[5] + ","), new IntWritable(1));
        }
    }

    public static class CustomMapper2 extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, Text> outputCollector, Reporter reporter) throws IOException {
            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");

            outputCollector.collect(new Text(singleRowData[0] + ","),
                    new Text(singleRowData[1] + "," + singleRowData[2]));
        }
    }

    public static class CustomReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        public void reduce(Text key, Iterator<IntWritable> iterator,
                           OutputCollector<Text, IntWritable> outputCollector, Reporter reporter) throws IOException {
            int frequencyForCountry = 0;
            while (iterator.hasNext()) {
                // replace type of value with the actual type of our value
                IntWritable value = iterator.next();
                frequencyForCountry += value.get();
            }
            outputCollector.collect(key, new IntWritable(frequencyForCountry));
        }
    }

    public static class CustomReducer2 extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            int max = 0;
            String argmax = "";
            while (values.hasNext()) {
                // replace type of value with the actual type of our value
                Text value = values.next();
                String[] vals = value.toString().split(",");
                int price = Integer.parseInt(vals[1].trim());
                if (price > max) {
                    max = price;
                    argmax = vals[0];
                }
            }
            output.collect(key, new Text(argmax));
        }
    }
}