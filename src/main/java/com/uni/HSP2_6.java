package com.uni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

public class HSP2_6 {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HSP2_6_Driver(), args);
        System.exit(res);
    }

    public static class HSP2_6_Driver extends Configured implements Tool {

        @Override
        public int run(String[] args) throws Exception {
            JobConf conf = new JobConf(getConf(), HSP2_6_Driver.class);

            conf.setJobName("hsp6_1");

            Path outputPath = new Path(args[1]);
            FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
            fs.delete(outputPath, true);

            FileInputFormat.setInputPaths(conf, new Path(args[0]));
            FileOutputFormat.setOutputPath(conf, new Path(args[1]));

            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(IntWritable.class);
            conf.setMapperClass(CustomMapper.class);
            conf.setReducerClass(CustomReducer.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf);

            return 0;
        }
    }

    public static class CustomMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

        private final static Text transaction_date = new Text();
        private final static IntWritable price = new IntWritable(0);

        public static boolean isNumeric(String strNum) {
            if (strNum == null) {
                return false;
            }
            try {
                double d = Double.parseDouble(strNum);
            } catch (NumberFormatException nfe) {
                return false;
            }
            return true;
        }

        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            String valueString = value.toString();
            String[] ClientCountryData = valueString.split(",");
            transaction_date.set(ClientCountryData[0].split(" ")[0]);
            String priceDate = ClientCountryData[2];
            if (isNumeric(priceDate)) {
                price.set(Integer.parseInt(priceDate));
            }
            output.collect(transaction_date, price);
        }
    }

    public static class CustomReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        private final IntWritable result = new IntWritable();

        public void reduce(Text t_key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output,
                           Reporter reporter) throws IOException {
            int sum = 0;
            while (values.hasNext()) {
                IntWritable val = values.next();
                sum += val.get();
            }
            result.set(sum);
            output.collect(t_key, result);
        }
    }
}