package com.uni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

public class HSP2_7 {
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HSP2_7_Driver(), args);
        System.exit(res);
    }

    public static class HSP2_7_Driver extends Configured implements Tool {

        @Override
        public int run(String[] args) throws Exception {
            JobConf conf = new JobConf(getConf(), HSP2_7_Driver.class);
            conf.setJobName("hsp2_7_1");

            Path outputPath = new Path(args[3]);
            FileSystem tmp_fs = FileSystem.get(new URI(outputPath.toString()), conf);
            tmp_fs.delete(outputPath, true);

            Path tmp_outputPath_2 = new Path(args[2]);
            tmp_fs = FileSystem.get(new URI(tmp_outputPath_2.toString()), conf);
            tmp_fs.delete(tmp_outputPath_2, true);

            Path tmp_outputPath_1 = new Path(args[1]);
            tmp_fs = FileSystem.get(new URI(tmp_outputPath_1.toString()), conf);
            tmp_fs.delete(tmp_outputPath_1, true);

            FileInputFormat.setInputPaths(conf, new Path(args[0]));
            FileOutputFormat.setOutputPath(conf, new Path(args[1]));

            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(Text.class);
            conf.setMapperClass(CustomMapper.class);
            conf.setReducerClass(CustomReducer.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf);

            JobConf conf2 = new JobConf(getConf(), HSP2_7_Driver.class);
            conf.setJobName("hsp2_7_2");

            FileInputFormat.setInputPaths(conf2, new Path(args[1]));
            FileOutputFormat.setOutputPath(conf2, new Path(args[2]));

            conf2.setOutputKeyClass(Text.class);
            conf2.setOutputValueClass(Text.class);
            conf2.setMapperClass(CustomMapper2.class);
            conf2.setReducerClass(CustomReducer2.class);
            conf2.setInputFormat(TextInputFormat.class);
            conf2.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf2);

            Path intermediateOutputPath = new Path(args[1]);
            FileSystem fs2 = FileSystem.get(new URI(intermediateOutputPath.toString()), conf);
            fs2.delete(intermediateOutputPath, true);

            JobConf conf3 = new JobConf(getConf(), HSP2_7_Driver.class);
            conf.setJobName("hsp2_7_3");

            FileInputFormat.setInputPaths(conf3, new Path(args[2]));
            FileOutputFormat.setOutputPath(conf3, new Path(args[3]));

            conf3.setOutputKeyClass(Text.class);
            conf3.setOutputValueClass(IntWritable.class);
            conf3.setMapperClass(CustomMapper3.class);
            conf3.setReducerClass(CustomReducer3.class);
            conf3.setInputFormat(TextInputFormat.class);
            conf3.setOutputFormat(TextOutputFormat.class);

            JobClient.runJob(conf3);

            Path intermediateOutputPath2 = new Path(args[2]);
            FileSystem fs3 = FileSystem.get(new URI(intermediateOutputPath2.toString()), conf);
            fs3.delete(intermediateOutputPath2, true);

            return 0;
        }
    }

    public static class CustomMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

        public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            if (key.get() == 0) {
                return;
            }
            String valueString = value.toString();
            String[] singleRowData = valueString.split(",");
            output.collect(new Text(singleRowData[7].trim() + ',' + singleRowData[1].trim()),
                    new Text(singleRowData[0] + ',' + singleRowData[2]));
        }
    }

    public static class CustomMapper2 extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

        public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {

            String[] rowData = value.toString().split("\t");
            output.collect(new Text(rowData[0].trim()), new Text(rowData[1].trim()));
        }
    }

    public static class CustomMapper3 extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {

            String[] rowData = value.toString().split("\t");
            output.collect(new Text(rowData[0].trim()), new IntWritable(Integer.parseInt(rowData[1].trim())));
        }
    }

    public static class CustomReducer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        public void reduce(Text t_key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            while (values.hasNext()) {
                // replace type of value with the actual type of our value
                String[] vals = values.next().toString().split(",");

                output.collect(t_key, new Text(vals[0].substring(7) + ',' + vals[1]));
            }
        }
    }

    public static class CustomReducer2 extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        public void reduce(Text t_key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            while (values.hasNext()) {
                // replace type of value with the actual type of our value
                String[] vals = values.next().toString().split(",");

                String[] date = vals[0].split(":"); // hour = date[0]
                String price = vals[1];

                int hour = Integer.parseInt(date[0]);

                if (hour >= 16 && hour <= 19) {
                    output.collect(t_key, new Text(price));
                }
            }
        }
    }

    public static class CustomReducer3 extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        public void reduce(Text t_key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output,
                           Reporter reporter) throws IOException {
            int totalPrice = 0;
            while (values.hasNext()) {
                // replace type of value with the actual type of our value
                IntWritable value = values.next();
                totalPrice += value.get();
            }
            output.collect(t_key, new IntWritable(totalPrice));
        }
    }
}